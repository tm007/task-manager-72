package ru.tsc.apozdnov.tm.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.tsc.apozdnov.tm.enumerated.RoleType;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "tm_role")
public class Role extends AbstractModel {

    @ManyToOne
    private User user;

    @Column(name = "role_type")
    @Enumerated(EnumType.STRING)
    private RoleType roleType = RoleType.USUAL;

    @Override
    public String toString() {
        return roleType.name();
    }

}
package ru.tsc.apozdnov.tm.config;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.tsc.apozdnov.tm.listener.LogListener;

import javax.jms.ConnectionFactory;

@Configuration
@ComponentScan("ru.tsc.apozdnov.tm")
public class ContexConfig {

    @Bean
    @NotNull
    public ConnectionFactory connectionFactory() {
        @NotNull final ActiveMQConnectionFactory factory =
                new ActiveMQConnectionFactory(ActiveMQConnection.DEFAULT_BROKER_URL);
        factory.setTrustAllPackages(true);
        return factory;
    }

    @Bean
    @NotNull
    public LogListener logListener() {
        return new LogListener();
    }

}

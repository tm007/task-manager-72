package ru.tsc.apozdnov.tm.component;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.apozdnov.tm.api.service.IReceiverService;
import ru.tsc.apozdnov.tm.listener.LogListener;
import ru.tsc.apozdnov.tm.service.ReceiverService;

@Component
public class Bootstrap {

    @NotNull
    @Autowired
    private IReceiverService receiverService;

    @NotNull
    @Autowired
    private LogListener logListener;

    public void run() {
        receiverService.receiveLog(logListener);
    }

}
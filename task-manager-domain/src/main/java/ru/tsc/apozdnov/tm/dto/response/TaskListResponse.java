package ru.tsc.apozdnov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apozdnov.tm.dto.model.TaskDtoModel;

import java.util.List;

@Getter
@NoArgsConstructor
public final class TaskListResponse extends AbstractResponse {

    @Nullable
    private List<TaskDtoModel> tasks;

    public TaskListResponse(@Nullable final List<TaskDtoModel> tasks) {
        this.tasks = tasks;
    }

}
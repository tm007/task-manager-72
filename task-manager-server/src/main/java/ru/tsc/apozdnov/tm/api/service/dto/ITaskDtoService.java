package ru.tsc.apozdnov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.tsc.apozdnov.tm.dto.model.TaskDtoModel;

import java.util.List;

public interface ITaskDtoService extends IUserOwnedDtoService<TaskDtoModel> {

    @NotNull
    List<TaskDtoModel> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

}